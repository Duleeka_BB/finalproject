package com.gn.app.dao.UserVerify;

import com.gn.app.model.userVerify.UserVerify;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface UserVerifyDao extends CrudRepository<UserVerify,String> {
    UserVerify findByUserEmail(String userEmail);
    UserVerify findByUserToken(String userToken);

}