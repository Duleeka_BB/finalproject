package com.gn.app.dao.citizenProfileTask.CitizenDetail;

import com.gn.app.constant.Gender;
import com.gn.app.dto.citizenProfileTask.CitizenDetail.CitizenDetailGraphDTO;
import com.gn.app.model.citizenProfileTask.CitizenDetail.CitizenDetail;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CitizenDetailDao extends DataTablesRepository<CitizenDetail, Integer> {
    /*@Query("select c from CitizenDetail c where c.CFamilyNo=:no")
    CitizenDetail findCitizenDetailById(@Param("no") String  CitizenDetail);*/

    @Query("select count(c.id)  from CitizenDetail c where c.citizenGender=:gender ")
    Integer findCitizenDetailByGender(@Param("gender")Gender gender);
}
