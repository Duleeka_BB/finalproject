package com.gn.app.dto.citizenProfileTask.CitizenDetail;

import com.gn.app.constant.Gender;
import com.gn.app.dto.BaseDTO;

public class CitizenDetailGraphDTO extends BaseDTO {

    String religion;
    Gender gender;
    String education;
    Integer count;

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

}
