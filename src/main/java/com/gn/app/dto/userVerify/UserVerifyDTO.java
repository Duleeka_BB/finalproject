package com.gn.app.dto.userVerify;

import com.gn.app.dto.BaseDTO;

import java.util.Date;

public class UserVerifyDTO extends BaseDTO {

    private Integer id;
    private String userEmail;
    private String userToken;
    private Date expireDate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }
}
