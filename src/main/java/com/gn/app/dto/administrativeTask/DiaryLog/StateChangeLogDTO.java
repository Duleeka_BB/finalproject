package com.gn.app.dto.administrativeTask.DiaryLog;

import com.gn.app.constant.StateOfTasks;
import com.gn.app.dto.BaseDTO;



public class StateChangeLogDTO extends BaseDTO {

    private Integer id;
    private String taskDescription;
    private StateOfTasks state = StateOfTasks.PENDING;
    private Integer diaryLogId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public StateOfTasks getState() {
        return state;
    }

    public void setState(StateOfTasks state) {
        this.state = state;
    }

    public Integer getDiaryLogId() {
        return diaryLogId;
    }

    public void setDiaryLogId(Integer diaryLogId) {
        this.diaryLogId = diaryLogId;
    }
}

