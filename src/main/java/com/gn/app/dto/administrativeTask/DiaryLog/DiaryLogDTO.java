package com.gn.app.dto.administrativeTask.DiaryLog;

import com.gn.app.constant.StateOfTasks;
import com.gn.app.dto.BaseDTO;
import com.gn.app.dto.citizenProfileTask.DonationDetail.DonationDetailDonationTypeDTO;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by hp on 1/31/2019.
 */

public class DiaryLogDTO extends BaseDTO {

    private Integer id;
    private String taskName;
    private String description;
    private StateOfTasks state= StateOfTasks.PENDING;
    private String startDate;
    private String finishDate;

    private List<StateChangeLogDTO> diaryLogDTOS = new ArrayList<>();


    public Integer getId() {
        return id;
    } public void setId(Integer id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    } public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }  public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }  public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public StateOfTasks getState() {
        return state;
    } public void setState(StateOfTasks state) {
        this.state = state;
    }

    public String getFinishDate() {
        return finishDate;
    } public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }


}
