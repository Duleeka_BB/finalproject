package com.gn.app.service.userVerify;


import com.gn.app.dto.userVerify.UserVerifyDTO;

public interface UserVerifyService {

    UserVerifyDTO findById(Integer id);

    void create(UserVerifyDTO dto);

    void sendemail(String email, Object userVerifyDTO);


    /*boolean sendVerificationMail(String userEmail, String token);*/

    void generateToken(String userToken);
}
