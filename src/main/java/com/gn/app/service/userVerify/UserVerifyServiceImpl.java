package com.gn.app.service.userVerify;



import com.gn.app.dao.UserVerify.UserVerifyDao;
import com.gn.app.dto.userVerify.UserVerifyDTO;
import com.gn.app.model.userVerify.UserVerify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;


@Service
    public class UserVerifyServiceImpl implements UserVerifyService {


    private JavaMailSender javaMailSender;

    @Autowired
    private UserVerifyDao userVerifyDao;

    @Autowired
    public UserVerifyServiceImpl(JavaMailSender javaMailSender)
    {
        this.javaMailSender = javaMailSender;

    }

    @Async
    public void sendemail(String email, Object userVerifyDTO) {
        SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
        simpleMailMessage.setTo(email);
        String userToken=UUID.randomUUID().toString();
        String url="locahost:8080/userRegisrter/token="+userToken+",email="+email;

        UserVerify userVerify = new UserVerify();
        UserVerifyDTO userVerifyDTO1=new UserVerifyDTO();
        userVerifyDTO1.setUserEmail(email);
        userVerifyDTO1.setUserToken(userToken);
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE,1);
        userVerifyDTO1.setExpireDate(calendar.getTime());
        save(userVerifyDTO1);

            simpleMailMessage.setSubject("Test mail");
            simpleMailMessage.setText("Test mail 123 , Please find url " + url);
            javaMailSender.send(simpleMailMessage);

        }

        private void save(UserVerifyDTO userVerifyDTO){

        UserVerify userVerify=new UserVerify();
        userVerify.setUserEmail(userVerifyDTO.getUserEmail());
        userVerify.setUserToken(userVerifyDTO.getUserToken());
        userVerify.setExpireDate(userVerifyDTO.getExpireDate());
            userVerifyDao.save(userVerify);

        }
    @Override
    public void create(UserVerifyDTO dto) {
    }

    @Override
    public void generateToken(String userToken) {

    }





    @Override
    public UserVerifyDTO findById(Integer id) {
        return null;
    }


    }
