package com.gn.app.service.citizenProfileTask.CitizenDetail;

import com.gn.app.constant.Gender;
import com.gn.app.dto.citizenProfileTask.CitizenDetail.CitizenDetailDTO;
import com.gn.app.dto.citizenProfileTask.CitizenDetail.CitizenDetailGraphDTO;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;

import java.util.List;

public interface CitizenDetailService {

    DataTablesOutput<CitizenDetailDTO> findAllDataTable(DataTablesInput input);

    DataTablesOutput<CitizenDetailGraphDTO> findAllByGender();

    List<CitizenDetailGraphDTO> findAllByGenderList();
    Integer findAllCountByGender(Gender gender);
    List<String> findAllNameByReligion();

    List<CitizenDetailDTO> findAll();

    CitizenDetailDTO create(CitizenDetailDTO citizenDetail);

    CitizenDetailDTO findById(Integer id);

    void delete(Integer id);
}
