package com.gn.app.security;

import java.util.Collection;
import java.util.List;

import com.gn.app.model.Settings.AgDivision;
import com.gn.app.model.Settings.GnDivisionRegister.GnDivisionRegister;
import com.gn.app.model.Settings.User;
import org.springframework.security.core.GrantedAuthority;


public class CurrentUser extends org.springframework.security.core.userdetails.User {

	private static final long serialVersionUID = -2385326206773475679L;

	private Integer userId;
	private String userName;
	private User userObj;
	private GnDivisionRegister gnDivisionRegister;
	private AgDivision agDivision;

	public CurrentUser(User user, Collection<GrantedAuthority> authorities, User obj) {
		super(user.getUsername(), user.getPassword(), authorities);
		userId = user.getId();
		userName = user.getUsername();
		userObj = obj;
	}

	public Integer getUserId() {
		return userId;
	}

	public User getUserObj() {
		return userObj;
	}

	public void setUserObj(User userObj) {
		this.userObj = userObj;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public GnDivisionRegister getGnDivisionRegister() {
		return gnDivisionRegister;
	}

	public void setGnDivisionRegister(GnDivisionRegister gnDivisionRegister) {
		this.gnDivisionRegister = gnDivisionRegister;
	}

	public AgDivision getAgDivision() {
		return agDivision;
	}

	public void setAgDivision(AgDivision agDivision) {
		this.agDivision = agDivision;
	}
}
