package com.gn.app.security;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by hp on 4/1/2019.
 */
public class NumberGeneratingUtil {

    public static String setServiceNextCode(String codePrefix, String nextNo) {
      //  final String nextCode = yearSuffix.toString().substring(2).concat("/").concat(StringUtils.leftPad(nextNo, 4, '0'));
     //   return nextCode;
        return null;
    }

    public static String getNextCode(String codePrefix,String lastCode) {
        Integer nextNo = 0;
        final List<String> codeList = Arrays.asList(lastCode.split("/"));
        if (!codeList.get(0).equalsIgnoreCase(codePrefix)) {
            nextNo = 1;
        } else {
            nextNo = Integer.parseInt(codeList.get(1)) + 1;
        }

        return codePrefix.concat("/").concat(StringUtils.leftPad(nextNo.toString(), 5, '0'));
    }
}