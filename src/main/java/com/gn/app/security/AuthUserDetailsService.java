package com.gn.app.security;

import com.gn.app.constant.Roles;
import com.gn.app.model.Settings.User;
import com.gn.app.service.AdministrativeTask.GnProfile.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by hp on 2/28/2019.
 */
@Service
public class AuthUserDetailsService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(AuthUserDetailsService.class);

    @Autowired
    private UserService userService;

    private org.springframework.security.core.userdetails.User springUser;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = getUserDetail(username);

        if (user == null) {
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }


//        final User userDetail = userDao.findOne(userCredentialDto.getUserId());

        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        GrantedAuthority grantedLevelAuthority;
        if(user.getRole().equals(Roles.ROLE_ADMIN)){
            grantedLevelAuthority = new SimpleGrantedAuthority("ROLE_ADMIN");
        } else if(user.getRole().equals(Roles.ROLE_AG)){
            grantedLevelAuthority = new SimpleGrantedAuthority("ROLE_AG");
        } else {
            grantedLevelAuthority = new SimpleGrantedAuthority("ROLE_GN");
        }

        authorities.add(grantedLevelAuthority);

        return new CurrentUser(user, authorities, user);

    }

    public List<GrantedAuthority> getAuthorities(Roles role) {

        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
        if (role == Roles.ROLE_ADMIN) {
            authList.add(new SimpleGrantedAuthority(Roles.ROLE_ADMIN.toString()));
        } else if (role ==Roles.ROLE_AG) {
            authList.add(new SimpleGrantedAuthority(Roles.ROLE_AG.toString()));
        } else if (role ==Roles.ROLE_GN) {
            authList.add(new SimpleGrantedAuthority(Roles.ROLE_GN.toString()));
        }

        return authList;
    }

    private User getUserDetail(String username) {

        User user = userService.findByUserName(username);
        if (user == null) {
            logger.warn("user '" + username + "' on null!");
        } else {
            logger.info(user.toString());
        }
        return user;
    }
}