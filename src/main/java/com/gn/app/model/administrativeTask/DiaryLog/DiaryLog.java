package com.gn.app.model.administrativeTask.DiaryLog;

import com.gn.app.constant.StateOfTasks;
import com.gn.app.model.BaseModel;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;


@Entity
@Table(name = "tbl_gn_diary")
public class DiaryLog extends BaseModel {
    private static final long serialVersionUID = 3466843752790052309L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)


    @Column(name = "id")private Integer id;
    @Column(name = "task_name")private String taskName;
    @Column(name = "description")private String description;
    @Column(name = "status")private StateOfTasks status;
    @Column(name = "start_date")private Date startDate;
    @Column(name = "finish_date")private Date finishDate;
    @OneToMany(mappedBy = "diaryLog",fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true)
    private Set<StateChangeLog> stateChangeLogs;


    public Integer getId() {
        return id;
    }public void setId(Integer id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }public void setDescription(String description) {
        this.description = description;
    }

    public StateOfTasks getStatus() {
        return status;
    }public void setStatus(StateOfTasks status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    } public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    } public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Set<StateChangeLog> getStateChangeLogs() {
        return stateChangeLogs;
    }  public void setStateChangeLogs(Set<StateChangeLog> stateChangeLogs) {
        this.stateChangeLogs = stateChangeLogs;
    }
}


