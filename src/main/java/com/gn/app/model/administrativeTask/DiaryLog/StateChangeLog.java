package com.gn.app.model.administrativeTask.DiaryLog;

import com.gn.app.model.BaseModel;


import javax.persistence.*;

@Entity
@Table(name = "tbl_state_change_log")
public class StateChangeLog extends BaseModel {
    private static final long serialVersionUID = 3466843752790052309L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)


    @Column(name = "id")
    private Integer id;

    @Column(name = "task_description")
    private String taskDescription;

    @JoinColumn(name = "diary_log_id")
    @ManyToOne(targetEntity = DiaryLog.class, fetch = FetchType.LAZY)
    private DiaryLog diaryLog;

    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }



    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public DiaryLog getDiaryLog() {
        return diaryLog;
    }

    public void setDiaryLog(DiaryLog diaryLog) {
        this.diaryLog = diaryLog;
    }


    public void add(StateChangeLog stateChangeLog) {
    }
}
