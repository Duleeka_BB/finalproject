package com.gn.app.constant;

import java.util.ArrayList;
import java.util.List;

public enum StateOfTasks {

    FINISHED(1,"Finished"),
    PENDING(2,"Pending"),
    STARTED(3,"Started"),
   IN_PROGRESS(4,"In Progress");

    private Integer stateId;
    private String stateName;

    StateOfTasks(Integer stateId, String stateName){
        setStateId(stateId);
        setStateName(stateName);
    }

    public static List<StateOfTasks> getAllStateOfTasks(){
        List<StateOfTasks> stateOfTasksList=new ArrayList<>();
        stateOfTasksList.add(FINISHED);
        stateOfTasksList.add(PENDING);
        return stateOfTasksList;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}
