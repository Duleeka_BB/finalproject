package com.gn.app.controller;


import com.gn.app.dto.userVerify.UserVerifyDTO;
import com.gn.app.service.userVerify.UserVerifyService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
@RequestMapping(EmailVerifyController.REQUEST_MAPPING_URL)
public class EmailVerifyController{
    public static final String REQUEST_MAPPING_URL = "/emailVerify";

@Autowired
    UserVerifyService userVerifyService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String emailVerifyPage(Model model) {
        setCommonData(model, new UserVerifyDTO() );
        return "emailVerify";

    }

    @RequestMapping(value = "/sendmail",method = RequestMethod.POST)
    public String sendEmail(Model model, @ModelAttribute UserVerifyDTO dto, Object userVerifyDTO) {
        userVerifyService.sendemail(dto.getUserEmail(), userVerifyDTO);
        setCommonData(model,dto);
        return "emailVerify";
    }

    @RequestMapping(value = "/verifymail",method = RequestMethod.POST)
    public String tokenGenerate(Model model, @ModelAttribute UserVerifyDTO dto) {
        userVerifyService.generateToken(dto.getUserToken());
        setCommonData(model,dto);
        return "emailVerify";
    }


    @RequestMapping(value = "/verifymail/token={token},email={email}",method = RequestMethod.POST)
    public String tokenGenerate(Model model, @PathVariable("token") String token, @PathVariable("email") String email, UserVerifyDTO dto) {
        userVerifyService.generateToken(dto.getUserToken());
        setCommonData(model,dto);
        return "emailVerify";
    }

    private void setCommonData(Model model, UserVerifyDTO dto)
    {
        model.addAttribute("userVerify",dto);
    }

}
