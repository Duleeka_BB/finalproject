package com.gn.app.controller.admnistrativeTask.stateChangeLog;

import com.gn.app.dto.administrativeTask.DiaryLog.StateChangeLogDTO;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping(StateChangeLogController.REQUEST_MAPPING_URL)
public class StateChangeLogController {
    public static final String REQUEST_MAPPING_URL = "/statechangelog";


    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String newStateChangeLog(Model model) {
        setCommonData(model, new StateChangeLogDTO());
        return "administrativeTask/diaryLog/administrative-task-diary-log";
    }

    private void setCommonData(Model model, StateChangeLogDTO dto) {
    }
}